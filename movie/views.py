from django.shortcuts import render

# Create your views here.

from django.http import HttpResponse


def find_all(request):
    return HttpResponse("所有的电影")


def find_by_id(request, id):
    return HttpResponse("电影-%s" % id)


def find_by_name(request):
    name = request.GET.get('name')
    return HttpResponse("电影-%s" % name)