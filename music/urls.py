# Author: zhouweixin
# @Time: 2019-07-08 18:35
# @File: urls.py
# @Description:

from django.urls import path
from . import views

urlpatterns = [
    path('find_all', views.find_all),
    path('find_by_id/<id>', views.find_by_id),
    path('find_by_order', views.find_by_order),
    path('find_by_order/<int:order>', views.find_by_order),
]