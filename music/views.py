from django.http import HttpResponse

music_list = [
    '画心',
    '两只老虎',
    '老乡',
    '人在旅途',
]


def find_all(request):
    return HttpResponse("所有音乐")


def find_by_id(request, id):
    return HttpResponse("音乐-%s" % id)


def find_by_order(request, order=0):
    return HttpResponse(music_list[order])