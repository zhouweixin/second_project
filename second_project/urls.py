"""second_project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.http import HttpResponse
import book.views as book
import movie.views as movie
import music.urls as music


def index(request):
    return HttpResponse("自定义首页")


def hello(request):
    return HttpResponse("Hello, zhouweixin")


urlpatterns = [
    path('admin/', admin.site.urls),

    # 设置自定义首页
    path('', index),

    # 写hello world
    path('hello', hello),

    path('book/find_all', book.find_all),
    path('movie/find_all', movie.find_all),
    path('movie/find_by_id/<int:id>', movie.find_by_id),
    path('movie/find_by_name', movie.find_by_name),

    path('music/', include(music)),
]
